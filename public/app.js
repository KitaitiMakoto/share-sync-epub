const uiConfig = {
  signInSuccessUrl: location.href,
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID
  ]
};

const ui = new firebaseui.auth.AuthUI(firebase.auth());

function initApp() {
  return new Promise((resolve, reject) => {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var uid = user.uid;
        var phoneNumber = user.phoneNumber;
        var providerData = user.providerData;
        user.getIdToken().then(function(accessToken) {
          document.getElementById('sign-in-status').textContent = 'Signed in';
          document.getElementById('sign-in').textContent = 'Sign out';
          document.getElementById('upload-form').hidden = false;
          document.getElementById('sign-in').addEventListener('click', () => {
            firebase.auth().signOut().then(() => location.href = location.href);
          });
          let userDataRef = firebase.firestore().collection('users').doc(uid);
          let epubsRef = userDataRef.collection('epubs');
          (async () => {
            let epubs = await epubsRef.get();
            let $epubs = document.getElementById('epubs');
            let $ul = $epubs.getElementsByTagName('ul')[0];
            epubs.forEach(epub => {
              let data = epub.data();
              let $li = document.createElement('li');
              $li.id = `epub-${epub.id}`;
              $li.append(data.name);
              let $downloadA = document.createElement('a');
              $downloadA.href = data.url;
              $downloadA.download = data.name;
              $downloadA.textContent = 'ダウンロード';
              let $readerA = document.createElement('a');
              let readerURL = new URL(location.origin + '/reader.html');
              readerURL.searchParams.append('book', data.url);
              $readerA.href = readerURL.href;
              $readerA.textContent = '表示';
              $readerA.target = 'reader';
              $li.append($readerA);
              $li.append($downloadA);
              $li.append(`（${data.createdAt.toDate().toLocaleString()}）`);
              $ul.append($li);
            });
            $epubs.hidden = false;
          })();
          document.getElementById('upload-form').addEventListener('submit', async (event) => {
            event.preventDefault();
            let data = new FormData(event.target);
            let file = data.get('epub-file');
            let createdAt = new Date(Date.now());
            let epubRef = await epubsRef.add({name: file.name, createdAt});
            let $submit = event.target.querySelector('[type="submit"]');
            $submit.disabled = true;
            let oldValue = $submit.value;
            $submit.value = 'アップロード中...';
            let snapshot = await firebase.storage().ref().child(`${uid}/${epubRef.id}.epub`).put(file)
            let downloadURL = await snapshot.ref.getDownloadURL();
            await epubRef.set({url: downloadURL}, {merge: true});
            $submit.value = oldValue;
            $submit.disabled = false;
            event.target.reset();
            let $ul = document.querySelector('#epubs ul');
            let $li = document.createElement('li');
            $li.id = `epub-${epubRef.id}`;
            $li.append(file.name);
            let $downloadA = document.createElement('a');
            $downloadA.href = downloadURL;
            $downloadA.download = file.name;
            $downloadA.textContent = 'ダウンロード';
            $li.append($downloadA);
            let $readerA = document.createElement('a');
            let readerURL = new URL(location.origin + '/reader.html');
            readerURL.searchParams.append('book', downloadURL);
            $readerA.href = readerURL.href;
            $readerA.textContent = '表示';
            $readerA.target = 'reader';
            $li.append($readerA);
            $li.append(`（${createdAt.toLocaleString()}）`);
            $ul.append($li);
            let res = await firebase.app().functions('asia-northeast1').httpsCallable('unpackEpub')({object: {
              bucket: snapshot.metadata.bucket,
              name: snapshot.metadata.fullPath,
              size: snapshot.metadata.size,
              contentType: snapshot.metadata.contentType
            }})
          });
        });
      } else {
        // User is signed out.
        document.getElementById('sign-in-status').textContent = 'Signed out';
        document.getElementById('sign-in').textContent = 'Sign in';
        ui.start('#app', uiConfig);
      }
      resolve(user);
    }, function(error) {
      ui.start('#app', uiConfig);
      reject(error);
    });
  });
};

(async function() {
  let res = await initApp();
})();
