const functions = require('firebase-functions');
const admin = require('firebase-admin');
const os = require('os');
const path = require('path');
const fs = require('fs');
const unzipper = require('unzipper');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

admin.initializeApp();

const CONTENT_TYPES = {
  '.xhtml': 'application/xhtml+xml',
  '.css': 'text/css',
  '.gif': 'image/gif',
  '.jpeg': 'image/jpeg',
  '.jpg': 'image/jpeg',
  '.png': 'image/png',
  '.svg': 'image/svg+xml',
  '.woff': 'font/woff',
  '.woff2': 'font/woff2',
  '.ttf': 'font/ttf',
  '.otf': 'font/otf',
  '.eot': 'application/vnd.ms-fontobject',
  '.xml': 'application/xml',
  '.opf': 'application/xml'
};

exports.unpackEpub = functions.region('asia-northeast1').https.onCall(async ({object}, context) => {
    if (path.extname(object.name) !== '.epub') return;

    const bucket = admin.storage().bucket(object.bucket);
    const tmpdir = os.tmpdir();
    const epubPath = path.join(tmpdir, path.basename(object.name));
    const dir = path.join(path.dirname(object.name), path.basename(object.name, '.epub'));
    await bucket.file(object.name).download({destination: epubPath});

    fs.createReadStream(epubPath)
        .pipe(unzipper.Parse())
        .on('entry', entry => {
            if (entry.type === 'File') {
                let ext = path.extname(entry.path).toLowerCase();
                let contentType = CONTENT_TYPES[ext] || 'application/octet-stream';
                entry.pipe(
                    admin.storage().bucket(object.bucket).file(path.join(dir, entry.path)).createWriteStream({metadata: {contentType}})
                )
                    .on('error', err => {
                        console.error(err);
                        // update status
                    })
                    .on('finish', () => {
                        // update status
                    })
            } else {
                entry.autodrain();
            }
        })
        .on('finish', () => fs.unlinkSync(epubPath));
});
